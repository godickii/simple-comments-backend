# Пример кода на backend сервиса комментариев

## Установка

    $ mkvirtualenv simple-comments-backend
    $ pip install -r requirements.txt
    $ src/manage.py migrate

## Использование

    $ src/manage.py runserver

Открыть http://localhost:8000 в браузере.