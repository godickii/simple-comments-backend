# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function, unicode_literals)

import factory

from comments.models import Comment, ExportComment


class CommentFactory(factory.DjangoModelFactory):
    """Фабрика комментариев."""

    owner_id = 1
    text = factory.Sequence(lambda n: 'Текст №-%s' % n)
    entity_type = factory.LazyAttribute(lambda this: this.parent and this.parent.entity_type or None)
    entity_id = factory.LazyAttribute(lambda this: this.parent and this.parent.entity_id or None)

    class Meta(object):
        model = Comment


class ExportCommentFactory(factory.DjangoModelFactory):
    """Фабрика объекта экспорта комментариев."""
    owner_id = 0

    class Meta(object):
        model = ExportComment
