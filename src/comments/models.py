# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function, unicode_literals)

from django.db import models
from django.conf import settings
from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager
from rest_framework.generics import get_object_or_404


class CommentManager(TreeManager):
    """Менеджер комментариев."""
    # Сначала отнаследовался от models.Manager и словил сложный баг с построением дерева.
    # Дело в том, что TreeManager даёт нам сортировку в глубину (depth-first ordering),
    # что позволяет строить дерево в один проход и только с такой сортировкой работает
    # mptt.utils.get_cached_trees.

    def make_root_comment(self, owner_id, entity_type, entity_id, text):
        """Создание корневого комментария."""
        return self.create(  # @UndefinedVariable
            owner_id=owner_id,
            text=text,
            entity_type=entity_type,
            entity_id=entity_id,
        )

    def make_child_comment(self, parent, owner_id, text):
        """Создание дочернего комментария."""
        return self.create(  # @UndefinedVariable
            owner_id=owner_id,
            parent=parent,
            text=text,
            entity_type=parent.entity_type,
            entity_id=parent.entity_id,
        )

    def get_collection(self, entity=None, comment_id=None, owner_id=None,
                       created_from=None, created_to=None, with_childs=False):
        """Возвращает комментарии, отфильтрованные заданным условием.

        :param entity: по родительской сущности, пара (ключ, значение)
        :param comment_id: по id родительского комментария
        :param owner_id: по id владельца
        :param created_from: от какой даты-времени
        :param created_to: до какой даты-времени
        :param with_childs: для поиска по entity и comment_id: с потомками
        """

        comments = self.all()
        if entity:
            assert len(entity) == 2
            entity_type, entity_id = entity
            comments = comments.filter(entity_type=entity_type, entity_id=entity_id)
            if not with_childs:
                comments = comments.filter(parent__isnull=True)

        if comment_id is not None:
            comments = comments.filter(pk=comment_id)
            if with_childs:
                comments = comments.get_descendants(include_self=True)

        if owner_id is not None:
            comments = comments.filter(owner_id=owner_id)

        if created_from:
            comments = comments.filter(created_at__gte=created_from)

        if created_to:
            comments = comments.filter(created_at__lte=created_to)

        return comments


class Comment(MPTTModel):
    """Элемент дерева комментариев.

    Для хранения дерева взят алгоритм Modified Preorder Tree Traversal
    https://www.caktusgroup.com/blog/2016/01/04/modified-preorder-tree-traversal-django/

    Он весьма тяжёл на запись:

    UPDATE "comments_comment"
    SET "lft" = CASE
            WHEN "lft" > 13
                THEN "lft" + 2
            ELSE "lft" END,
        "rght" = CASE
            WHEN "rght" > 13
                THEN "rght" + 2
            ELSE "rght" END
    WHERE "tree_id" = 2
      AND ("lft" > 13 OR "rght" > 13)

    но очень лёгок на чтение. Нас это устраивает - комментарии гораздо чаще читаются, чем пишутся.
    Как и код, кстати.

    Materialized Path тоже подошёл бы, но его вложенность ограничена размером поля. А размер поля
    ограничен индексами.

    Проще всего использовать батарейку django-mptt, но тогда наша модель будет немного избыточна.

    Вопрос в том, как хранить сущность, к которой привязывается комментарий верхнего уровня:

    1. Как отдельную модель, но это может помешать использованию django-mptt, если мы хотим
    иметь общее дерево.

    2. Хранить её в этой же таблице, только как пару (тип-сущности, значение-сущности) без текста
    комментария, и тогда комментарии первого уровня были бы все в одном дереве с общим корнем,
    несколько обощилась бы логика, но сама модель была бы менее логичной.

    3. Вообще не хранить сущность отдельно, просто сохранять для комментариев идентификаторы
    корневой сущности.

    Выбран третий вариант, но второй, имхо, ничем не хуже.
    """

    owner_id = models.IntegerField(
        'id владельца (число)',
        db_index=True)
    entity_type = models.CharField(
        'тип корневой сущности',
        max_length=50,
        null=True,
        blank=True)
    entity_id = models.IntegerField(
        'идентификатор корневой сущности (число)',
        null=True,
        blank=True,
        db_index=True)
    parent = TreeForeignKey(
        'self',
        verbose_name='Родительский комментарий',
        null=True,
        blank=True,
        related_name='childrens',
        db_index=True)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    objects = CommentManager()

    class Meta:
        index_together = [
            ["entity_type", "entity_id"],
        ]

    def __str__(self):
        return '<%s %d>' % (self.__class__.__name__, self.pk)


class ExportComment(models.Model):
    """Операция выгрузки комментариев."""
    filter_by_created_from = models.DateTimeField(null=True)
    filter_by_created_to = models.DateTimeField(null=True, auto_now_add=True)
    owner_id = models.IntegerField('id владельца выгрузки (число)')
    filter_by_owner_id = models.IntegerField('фильтр по id владельца (число)', null=True)
    filter_by_entity_type = models.CharField('тип сущности', max_length=50, null=True)
    filter_by_entity_id = models.IntegerField('идентификатор сущности', null=True)

    def get_filtered_comments(self):
        by_entity = None
        by_comment_id = None
        if self.filter_by_entity_type and self.filter_by_entity_id is not None:
            if self.filter_by_entity_type == settings.ENTITY_TYPE_COMMENT:
                by_comment_id = get_object_or_404(Comment, pk=self.filter_by_entity_id).pk
            else:
                by_entity = (self.filter_by_entity_type, self.filter_by_entity_id)

        return Comment.objects.get_collection(
            entity=by_entity,
            comment_id=by_comment_id,
            owner_id=self.filter_by_owner_id,
            created_from=self.filter_by_created_from,
            created_to=self.filter_by_created_to,
            with_childs=by_entity or by_comment_id)
