# -*- coding: utf-8 -*-
"""Serializers for django-rest-framework."""
from __future__ import (absolute_import, division, print_function, unicode_literals)

from rest_framework import serializers

from comments.models import Comment, ExportComment


class CommentSerializerForReading(serializers.ModelSerializer):
    """Сериализатор Comment для чтения."""
    edit_url = serializers.HyperlinkedIdentityField(view_name='comments:edit')

    class Meta:
        model = Comment
        fields = ('id', 'owner_id', 'entity_type', 'entity_id', 'text', 'parent', 'edit_url')


class CommentSerializerForCreation(serializers.ModelSerializer):
    """Сериализатор Comment для создания."""
    class Meta:
        model = Comment
        fields = ('id', 'owner_id', 'entity_type', 'entity_id', 'text')


class CommentSerializerForExport(serializers.ModelSerializer):
    """Сериализатор Comment для экспорта."""
    class Meta:
        model = Comment
        fields = ('id', 'owner_id', 'entity_type', 'entity_id', 'text', 'parent')


class CommentSerializerForEditing(serializers.ModelSerializer):
    """Сериализатор Comment для редактирования."""
    class Meta:
        model = Comment
        fields = ('id', 'owner_id', 'entity_type', 'entity_id', 'text')
        extra_kwargs = {
            'owner_id': {'read_only': True},
            'entity_type': {'read_only': True},
            'entity_id': {'read_only': True},
        }


class ExportCommentSerializer(serializers.ModelSerializer):
    """Сериализатор ExportComment для создания."""
    perform_export_url = serializers.HyperlinkedIdentityField(view_name='comments:perform-export')

    filter_by_created_from = serializers.DateTimeField(
        format='%Y-%m-%d %H:%M',
        input_formats=['%Y-%m-%d %H:%M'],
        required=False,
        label='Начиная с даты-времени (в формате «ГГГГ-ММ-ДД ЧЧ:ММ»)')
    filter_by_created_to = serializers.DateTimeField(
        format='%Y-%m-%d %H:%M',
        input_formats=['%Y-%m-%d %H:%M'],
        required=False,
        label='Заканчивая датой-временем (в формате «ГГГГ-ММ-ДД ЧЧ:ММ»)')

    class Meta:
        model = ExportComment
        fields = ('id', 'owner_id',
                  'filter_by_created_from', 'filter_by_created_to', 'filter_by_owner_id',
                  'filter_by_entity_type', 'filter_by_entity_id',
                  'perform_export_url')
