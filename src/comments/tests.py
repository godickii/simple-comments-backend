# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function, unicode_literals)

import datetime
import pprint
import json

from django.test import TestCase
from django.test.client import Client
from django.conf import settings
from django.utils import timezone
from rest_framework.reverse import reverse
from rest_framework import status

from comments.models import Comment, ExportComment
from comments.factories import CommentFactory, ExportCommentFactory

# Create your tests here.
pp = pprint.PrettyPrinter(indent=4).pprint


class TreeDepthTest(TestCase):
    """Да, MPTT нам гарантирует бесконечную вложенность. Но я всё равно хочу проверить."""

    def test(self):
        """Цепочка из 100 комментариев, извлечение из БД за 1 запрос."""
        current_comment = None
        for i in range(100):
            current_comment = CommentFactory(parent=current_comment)
            # Кстати, на слишком большой вложенности падает по глубине стека
            # при обновлении кэша родительских объектов, если не удалить
            # кэш принудительно:
            # del current_comment._parent_cache

        root = Comment.objects.get(parent__isnull=True)
        with self.assertNumQueries(1):
            child_comments = list(root.get_descendants(include_self=False))

        self.assertEqual(len(child_comments), 99)


class APITestCase(TestCase):
    """Базовый класс для тестирования вьюшек."""

    json_headers = {'HTTP_ACCEPT': 'application/json', 'HTTP_HOST': 'testhost'}

    def setUp(self):
        """Configuration that runs for every test."""
        self.json_client = Client(**self.json_headers)


class RootTest(APITestCase):
    """Не упала вьюшка с описаниями API - и то хорошо."""
    view_name = 'comments:root'

    def test(self):
        """Спасибо, что не пятисотка."""
        client = Client()
        response = client.get(reverse(self.view_name), HTTP_ACCEPT='text/html')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class CreateCommentTest(APITestCase):
    view_name = 'comments:create'

    def test_get_description(self):
        """Получение описания ресурса."""
        client = Client()
        response = client.get(reverse(self.view_name), HTTP_ACCEPT='text/html')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_post_incorrect_data(self):
        """Некорректный запрос."""
        post_data = {
            'owner_id': 1,
        }
        response = self.json_client.post(reverse(self.view_name), post_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_root_comment(self):
        """Создание комментария первого уровня."""
        post_data = {
            'owner_id': 1,
            'text': 'Ыыыть!',
            'entity_type': 'blog-post',
            'entity_id': 123
        }
        response = self.json_client.post(reverse(self.view_name), post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        comment_id = json.loads(response.content)['id']
        comment = Comment.objects.get(pk=comment_id)
        self.assertEqual(comment.owner_id, post_data['owner_id'])
        self.assertEqual(comment.entity_type, post_data['entity_type'])
        self.assertEqual(comment.entity_id, post_data['entity_id'])
        self.assertEqual(comment.parent, None)
        self.assertNotEqual(comment.created_at, None)
        self.assertNotEqual(comment.modified_at, None)

    def test_create_child_absent_comment(self):
        """Создание дочернего комментария к комментарию, которого нет."""
        post_data = {
            'owner_id': 1,
            'text': 'Яяять!',
            'entity_type': settings.ENTITY_TYPE_COMMENT,
            'entity_id': 123
        }
        response = self.json_client.post(reverse(self.view_name), post_data)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_child_comment(self):
        """Создание дочернего комментария к другому комментарию."""
        parent_comment = CommentFactory(entity_type='blog-post', entity_id=1234)

        post_data = {
            'owner_id': 12345,
            'text': 'Ыыыть!',
            'entity_type': settings.ENTITY_TYPE_COMMENT,
            'entity_id': parent_comment.id
        }
        response = self.json_client.post(reverse(self.view_name), post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        comment_id = json.loads(response.content)['id']
        comment = Comment.objects.get(pk=comment_id)
        self.assertEqual(comment.owner_id, post_data['owner_id'])
        self.assertEqual(comment.entity_type, parent_comment.entity_type)
        self.assertEqual(comment.entity_id, parent_comment.entity_id)
        self.assertEqual(comment.parent, parent_comment)
        self.assertNotEqual(comment.created_at, None)
        self.assertNotEqual(comment.modified_at, None)


class EditCommentTest(APITestCase):
    """Тесты редактирование комментария."""
    view_name = 'comments:edit'

    def test_edit_comment(self):
        """Редактирование комментария."""
        comment0 = CommentFactory(entity_type='blog-post', entity_id=1234)

        put_data = {
            'entity_type': {'read_only': True},
            'text': 'Уууть!',
            'entity_type': 'aaa',
            'entity_id': 111
        }
        response = self.json_client.put(
            reverse(self.view_name, kwargs={'pk': comment0.pk}),
            json.dumps(put_data),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        comment_id = json.loads(response.content)['id']
        comment = Comment.objects.get(pk=comment_id)
        # Ничего не изменилось, кроме ...
        self.assertEqual(comment.owner_id, comment0.owner_id)
        self.assertEqual(comment.entity_type, comment0.entity_type)
        self.assertEqual(comment.entity_id, comment0.entity_id)
        self.assertEqual(comment.parent, comment0.parent)
        # Кроме текста комментария
        self.assertEqual(comment.entity_id, comment0.entity_id)
        self.assertNotEqual(comment.text, comment0.text)
        self.assertEqual(comment.text, put_data['text'])

    def test_delete_leaf_comment(self):
        """Удаление комментария, у которого нет дочерних."""
        leaf_comment = CommentFactory(entity_type='blog-post', entity_id=1234)
        response = self.json_client.delete(
            reverse(self.view_name, kwargs={'pk': leaf_comment.pk}),
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertRaises(Comment.DoesNotExist, Comment.objects.get, pk=leaf_comment.pk)  # @UndefinedVariable

    def test_delete_not_leaf_comment(self):
        """Удаление комментария, у которого есть дочерние, невомозможно."""
        not_leaf_comment = CommentFactory(entity_type='blog-post', entity_id=1234)
        leaf_comment = CommentFactory(parent=not_leaf_comment)
        response = self.json_client.delete(
            reverse(self.view_name, kwargs={'pk': not_leaf_comment.pk}),
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        Comment.objects.get(pk=leaf_comment.pk)  # Комментарий есть, исключения нет


class TopLevelCommentsListTest(APITestCase):
    """Тесты на получение комментариев первого уровня с пагинацией."""

    view_name = 'comments:top-level-list'

    def test_get_top_level_comments(self):
        """Получаются комментарии первого уровня и не получаются остальные."""
        params = {'entity_type': 'blog-post', 'entity_id': 1234}
        root_comment = CommentFactory(**params)
        with self.assertNumQueries(2):
            response = self.json_client.get(reverse(self.view_name), params)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = json.loads(response.content)['results']
        self.assertEqual(len(results), 1)

        # Новый комментарий первого уровня - размер ответа изменился
        root_comment_2 = CommentFactory(**params)  # noqa
        with self.assertNumQueries(2):
            response = self.json_client.get(reverse(self.view_name), params)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = json.loads(response.content)['results']
        self.assertEqual(len(results), 2)

        # Новый комментарий второго уровня - размер ответа не изменился
        non_root_comment = CommentFactory(parent=root_comment)  # noqa
        with self.assertNumQueries(2):
            response = self.json_client.get(reverse(self.view_name), params)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = json.loads(response.content)['results']
        self.assertEqual(len(results), 2)


class ChildCommentsListTest(APITestCase):
    """Тесты на получение поддерева комметариев."""

    view_name = 'comments:child-comments-list'

    def test_get_child_comments(self):
        """Получение дочерних комментариев, не только непосредственных."""
        params = {'entity_type': 'blog-post', 'entity_id': 1234}

        # небольшое дерево в форме лямбды
        root_comment = CommentFactory(**params)
        descendant_comment_1 = CommentFactory(parent=root_comment)  # noqa
        descendant_comment_2 = CommentFactory(parent=root_comment)
        leaf_comment_21 = CommentFactory(parent=descendant_comment_2)

        # Несколько сущностей, которые не должны попасть в выборку
        root_comment_other_subtree = CommentFactory(**params)
        child_comment_other_subtree = CommentFactory(parent=root_comment_other_subtree, **params)  # noqa

        with self.assertNumQueries(2):
            response = self.json_client.get(reverse(self.view_name, kwargs={'pk': root_comment.pk}))

        results = json.loads(response.content)
        self.assertEqual(
            set([record['id'] for record in results]),
            set([comment.id for comment in (descendant_comment_1, descendant_comment_2, leaf_comment_21)])
        )


class CommentsTreeTest(APITestCase):
    """Тесты на получение поддерева комметариев."""

    view_name = 'comments:get-subtree'

    def setUp(self):
        super(CommentsTreeTest, self).setUp()
        self.make_sample_tree()

    def make_sample_tree(self):  # noqa
        """Создаёт небольшое дерево."""
        self.params = {'entity_type': 'blog-post', 'entity_id': 1234}
        self.comment_1 = CommentFactory(text='1', **self.params)
        self.comment_1_1 = CommentFactory(parent=self.comment_1, text='1_1')

        CommentFactory(entity_type='some-other-entity', entity_id=1)  # мусор

        self.comment_1_2 = CommentFactory(parent=self.comment_1, text='1_2')
        self.comment_1_2_1 = CommentFactory(parent=self.comment_1_2, text='1_2_1')

        CommentFactory(parent=CommentFactory(
            entity_type='some-other-entity',
            entity_id=1))  # мусор

        self.comment_1_2_2 = CommentFactory(parent=self.comment_1_2, text='1_2_2')
        self.comment_1_3 = CommentFactory(parent=self.comment_1, text='1_3')
        self.comment_2 = CommentFactory(text='2', **self.params)

    @classmethod
    def get_only_text_tree_from_response(cls, content):
        """Оставляет в ответе только идентификаторы и список подэлементов."""
        if isinstance(content, list):
            result = [cls.get_only_text_tree_from_response(c) for c in content]
        elif isinstance(content, dict):
            result = {'text': content['text']}
            if 'childs' in content:
                result['childs'] = [cls.get_only_text_tree_from_response(c) for c in content['childs']]
        else:
            raise NotImplementedError(type(content))  # pragma: no cover

        return result

    def test_wrong_request(self):
        """Ошибочный запрос."""
        response = self.json_client.get(reverse(self.view_name))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_leaf(self):
        """Получение хвоста дерева."""
        response = self.json_client.get(reverse(self.view_name), {'comment_id': self.comment_1_2_1.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.get_only_text_tree_from_response(json.loads(response.content))
        self.assertEqual(tree, [{'text': '1_2_1'}])

        response = self.json_client.get(reverse(self.view_name), {'comment_id': self.comment_1_2_2.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.get_only_text_tree_from_response(json.loads(response.content))
        self.assertEqual(tree, [{'text': '1_2_2'}])

    def test_get_middle_level_tree(self):
        """Получение серединки дерева."""
        response = self.json_client.get(reverse(self.view_name), {'comment_id': self.comment_1_2.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.get_only_text_tree_from_response(json.loads(response.content))
        self.assertEqual(tree, [{'text': '1_2', 'childs': [{'text': '1_2_1'}, {'text': '1_2_2'}]}])

    def test_get_tree_from_top_level_comment(self):
        """Получение дерева от корнеквого комментария."""
        response = self.json_client.get(reverse(self.view_name), {'comment_id': self.comment_1.pk})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.get_only_text_tree_from_response(json.loads(response.content))
        self.assertEqual(
            tree,
            [{'text': '1', 'childs': [
                {'text': '1_1'}, {'text': '1_2', 'childs': [
                    {'text': '1_2_1'}, {'text': '1_2_2'}]},
                {'text': '1_3'}]}]
        )

    def test_get_tree_from_entity(self):
        """Получение дерева от корневой сущности."""

        response = self.json_client.get(reverse(self.view_name), self.params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.get_only_text_tree_from_response(json.loads(response.content))
        self.assertEqual(
            tree,
            [{'text': '1', 'childs':
                [{'text': '1_1'},
                 {'text': '1_2', 'childs':
                    [{'text': '1_2_1'}, {'text': '1_2_2'}]},
                 {'text': '1_3'}]},
             {'text': '2'}]
        )


class FilterByUserTest(APITestCase):
    """Тесты на фильтр по пользователю."""

    view_name = 'comments:filter-by-user'

    def test_filter_by_user(self):
        """Получение комментариев по пользователю."""
        user1 = 500
        user2 = 501
        params = {'entity_type': 'blog-post', 'entity_id': 1234}

        # небольшое дерево в форме лямбды под корневым комментарием
        root_comment = CommentFactory(owner_id=user1, **params)
        descendant_comment_1 = CommentFactory(parent=root_comment, owner_id=user1)
        descendant_comment_2 = CommentFactory(parent=root_comment, owner_id=user2)
        descendant_comment_2_1 = CommentFactory(parent=descendant_comment_2, owner_id=user1)
        descendant_comment_2_2 = CommentFactory(parent=descendant_comment_2, owner_id=user2)

        with self.assertNumQueries(1):
            response = self.json_client.get(reverse(self.view_name, kwargs={'owner_id': user1}))

        results = json.loads(response.content)
        self.assertEqual(
            set([record['id'] for record in results]),
            set([comment.id for comment in (root_comment, descendant_comment_1, descendant_comment_2_1)])
        )

        with self.assertNumQueries(1):
            response = self.json_client.get(reverse(self.view_name, kwargs={'owner_id': user2}))

        results = json.loads(response.content)
        self.assertEqual(
            set([record['id'] for record in results]),
            set([comment.id for comment in (descendant_comment_2, descendant_comment_2_2)])
        )


class CreateExportCommentTest(APITestCase):
    """Создание объекта экспорта."""

    view_name = 'comments:create-export'

    def test_create_export_by_date(self):
        """Создание экспорта по дате."""
        post_data = {
            'owner_id': 5,
            'filter_by_created_from': '2016-01-01 01:01',
        }
        response = self.json_client.post(reverse(self.view_name), post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        export = ExportComment.objects.get(pk=json.loads(response.content)['id'])
        self.assertEqual(export.owner_id, 5)
        # Время «от» - указанное
        self.assertEqual(export.filter_by_created_from.strftime('%Y-%m-%d %H:%M'), '2016-01-01 01:01')
        # Время «до» - не пустое (равно моменту создания объекта)
        self.assertTrue(export.filter_by_created_to)
        # Остальное - пусто
        self.assertFalse(export.filter_by_owner_id)
        self.assertFalse(export.filter_by_entity_type)
        self.assertFalse(export.filter_by_entity_id)

    def test_create_export_by_entity_type(self):
        """Создание экспорта по дате."""
        post_data = {
            'owner_id': 5,
            'filter_by_entity_type': 'blog-post',
            'filter_by_entity_id': '333',
        }
        response = self.json_client.post(reverse(self.view_name), post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        export = ExportComment.objects.get(pk=json.loads(response.content)['id'])
        self.assertEqual(export.filter_by_entity_type, 'blog-post')
        self.assertEqual(export.filter_by_entity_id, 333)
        self.assertEqual(export.owner_id, 5)
        # Время «от» отсутствуеет
        self.assertFalse(export.filter_by_created_from)
        # Время «до» - не пустое (равно моменту создания объекта)
        self.assertTrue(export.filter_by_created_to)
        # Остальное - пусто
        self.assertFalse(export.filter_by_owner_id)


class PerformExportCommentTest(APITestCase):
    """Исполнениее объекта экспорта."""

    view_name = 'comments:perform-export'

    def get_exported_comments_ids(self, export):
        """Делает запрос на вьюшку и возвращает множество id результатов."""
        response = self.json_client.get(reverse(self.view_name, kwargs={'pk': export.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = json.loads(response.content)
        return set([record['id'] for record in results])

    def test_not_exports_future_comments(self):
        """Созданные в будущем комментарии не попадают в экспорт."""
        export = ExportCommentFactory()
        self.assertTrue(export.filter_by_created_to)
        past_comment = CommentFactory(entity_type="a", entity_id=1)
        past_comment.created_at -= datetime.timedelta(days=1)
        past_comment.save()
        future_comment = CommentFactory(entity_type="a", entity_id=1)
        future_comment.created_at += datetime.timedelta(days=1)
        future_comment.save()

        results = self.get_exported_comments_ids(export)
        self.assertEqual(results, set([past_comment.pk]))

    def test_export_by_user(self):
        """Тест выгрузки по пользователям."""
        export = ExportCommentFactory(filter_by_owner_id=1)
        export.filter_by_created_to = None
        export.save()

        comment1 = CommentFactory(entity_type="a", entity_id=1, owner_id=1)
        comment2 = CommentFactory(entity_type="a", entity_id=1, owner_id=2)  # noqa
        comment21 = CommentFactory(entity_type="a", entity_id=1, owner_id=2)  # noqa

        results = self.get_exported_comments_ids(export)
        self.assertEqual(results, set([comment1.pk]))

    def test_export_by_entity(self):
        """Тест выгрузки по комментариям к стороннй сущности."""
        export = ExportCommentFactory(
            filter_by_entity_type="b",
            filter_by_entity_id=1
        )
        export.filter_by_created_to = None
        export.save()

        comment1 = CommentFactory(entity_type="a", entity_id=1)  # noqa
        comment2 = CommentFactory(entity_type="b", entity_id=1)
        comment21 = CommentFactory(entity_type="b", entity_id=1, parent=comment2)

        results = self.get_exported_comments_ids(export)
        self.assertEqual(results, set([comment2.pk, comment21.pk]))

    def test_export_by_comment(self):
        """Тест выгрузки по комментариям к комментарию."""
        comment1 = CommentFactory(entity_type="a", entity_id=1)  # noqa
        comment2 = CommentFactory(entity_type="b", entity_id=1)
        comment21 = CommentFactory(parent=comment2)
        comment211 = CommentFactory(parent=comment21)

        export = ExportCommentFactory(
            filter_by_entity_type=settings.ENTITY_TYPE_COMMENT,
            filter_by_entity_id=comment21.pk
        )
        export.filter_by_created_to = None
        export.save()

        results = self.get_exported_comments_ids(export)
        self.assertEqual(results, set([comment21.pk, comment211.pk]))

    def test_export_by_created_from(self):
        """Созданные в будущем комментарии не попадают в экспорт."""
        export = ExportCommentFactory(
            filter_by_created_from=timezone.now() - datetime.timedelta(days=5)
        )
        export.filter_by_created_to = None
        export.save()

        old_comment = CommentFactory(entity_type="a", entity_id=1)
        old_comment.created_at -= datetime.timedelta(days=10)
        old_comment.save()
        current_comment = CommentFactory(entity_type="a", entity_id=1)
        current_comment.created_at -= datetime.timedelta(days=1)
        current_comment.save()

        results = self.get_exported_comments_ids(export)
        self.assertEqual(results, set([current_comment.pk]))


class ExportCommentListTest(APITestCase):
    """Тест на получение истории выгрузок по пользователю."""

    view_name = 'comments:exports-list'

    def test_filters_by_owner(self):
        """Список выгрузки фильтруется по владельцу выгрузки."""
        ExportCommentFactory(owner_id=1)

        response = self.json_client.get(reverse(self.view_name, kwargs={'pk': 2}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = json.loads(response.content)
        self.assertEqual(len(results), 0)

        ExportCommentFactory(owner_id=2)

        response = self.json_client.get(reverse(self.view_name, kwargs={'pk': 2}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = json.loads(response.content)
        self.assertEqual(len(results), 1)
