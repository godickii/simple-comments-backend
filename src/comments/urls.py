# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function, unicode_literals)

from django.conf.urls import url
from comments import views

urlpatterns = [
    url(r'^$', views.RootView.as_view(), name='root'),
    url(r'^create/$', views.CreateCommentView.as_view(), name='create'),
    url(r'^edit/(?P<pk>[0-9]+)/$', views.EditCommentView.as_view(), name='edit'),
    url(r'^top-level-list/$', views.TopLevelCommentsListView.as_view(), name='top-level-list'),
    url(r'^child-comments-list/(?P<pk>[0-9]+)/$',
        views.ChildCommentsListView.as_view(), name='child-comments-list'),
    url(r'^subtree/$', views.CommentsTreeView.as_view(), name='get-subtree'),
    url(r'^by-user/(?P<owner_id>[0-9]+)/$', views.FilterByUserView.as_view(), name='filter-by-user'),
    url(r'^create-export/$', views.CreateExportCommentView.as_view(), name='create-export'),
    url(r'^perform-export/(?P<pk>[0-9]+)/file.(?P<format>\S+)?$',
        views.PerformExportCommentView.as_view(), name='perform-export'),
    url(r'^exports-list/(?P<pk>[0-9]+)/$',
        views.ExportCommentListView.as_view(), name='exports-list'),
]
