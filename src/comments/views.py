# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function, unicode_literals)

from django.conf import settings
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView,\
    get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.reverse import reverse
from rest_framework.utils import formatting
from rest_framework import status
from rest_framework.pagination import LimitOffsetPagination

from comments.models import Comment, ExportComment
from comments.serializers import CommentSerializerForCreation, CommentSerializerForEditing,\
    CommentSerializerForReading, CommentSerializerForExport, ExportCommentSerializer
from mptt.utils import get_cached_trees


class RootView(APIView):
    """Краткая справка по API."""

    def get_view_description(self, html=False):
        doc = """
        # Backend для сервиса комментариев

        - Каждый комментарий имеет привязку к определенному пользователю
        - Комментарии могут редактироваться и удаляться
        - Удаление возможно только если у комментария нет дочерних комментариев
        - У каждого комментария есть дата создания и время последнего редактирования
        - Коментарии имеют древовидную струткуру (есть возможность оставлять
        комментарии на комментарии с неограниченой степенью вложенности)
        - Каждой комментарий имеет привязку к определенной сущности (пост в блоге,
        страница пользователся, другой комментарий и т.п.), которая однозначно
        идентифицируется парой значений (идентификатор типа сущности, идентификатор
        сущности)

        ## Создание комментария к определённой сущности

        <{create-comment}>

        ## Редактирование и удаление комментария по идентификатору
        <{edit-comment}> , где 1 - id комментария

        ## Получение комментариев первого уровня для определенной сущности с пагинацией
        <{top-level-comments-list}>

        ## Получение всех дочерних для заданного комментариев
        <{child-comments-list}> , где 1 - id комментария

        ## Получение полной ветки комментариев с указанием корня

        В виде дерева.

        <{get-subtree}>

        ## Получение истории комментариев определенного пользователя.
        <{filter-by-user}> , где 1 - id пользователя

        ## Выгрузка в файл

        Выгрузка всей истории комментариев по
        пользователю или сущности с возможностью указания интервала времени,
        в котором был создан комментарий пользователя (если не задан ­ выводить
        всё). Время ответа на ​первичный запрос не должно зависеть от объема
        данных в итоговой выгрузке.

        Выгрузка состоит из двух этапов:

        - создание задачи на экспорт <{create-export}>
        - исполнение задачи на экспорт <{perform-export}> , где 1 - id задачи

        ## Просмотр истории всех выгрузок пользователя

        <{exports-list}> , где 1 - id пользователя
        """

        args = {
            'create-comment': reverse('comments:create', request=self.request),
            'edit-comment': reverse('comments:edit', kwargs={'pk': '1'}, request=self.request),
            'top-level-comments-list': reverse('comments:top-level-list', request=self.request),
            'child-comments-list': reverse('comments:child-comments-list', kwargs={'pk': '1'},
                                           request=self.request),
            'get-subtree': reverse('comments:get-subtree', request=self.request),
            'filter-by-user': reverse('comments:filter-by-user', request=self.request,
                                      kwargs={'owner_id': '1'}),
            'create-export': reverse('comments:create-export', request=self.request),
            'perform-export': reverse('comments:perform-export', kwargs={'pk': '1', 'format': 'xml'},
                                      request=self.request),
            'exports-list': reverse('comments:exports-list', request=self.request, kwargs={'pk': '1'}),
        }
        return formatting.markup_description(formatting.dedent(doc.format(**args)))


class CreateCommentView(CreateAPIView):
    """Создание комментария к определённой сущности."""

    serializer_class = CommentSerializerForCreation

    def get_view_description(self, html=False):
        doc = """Создание комментария к определённой сущности.

        Для создания комментария к комментарию укажи тип родительской сущности
        «{entity-type-comment}» (задаётся в настройках проекта)."""

        args = {
            'entity-type-comment': settings.ENTITY_TYPE_COMMENT
        }

        return formatting.markup_description(formatting.dedent(doc.format(**args)))

    def post(self, request):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            if serializer.validated_data['entity_type'] == settings.ENTITY_TYPE_COMMENT:
                # Особое поведение для случая, когда комментарий создаётся к комментарию
                parent_comment = get_object_or_404(Comment, pk=serializer.validated_data['entity_id'])
                comment = Comment.objects.make_child_comment(
                    parent=parent_comment,
                    owner_id=serializer.validated_data['owner_id'],
                    text=serializer.validated_data['text'])
            else:
                comment = Comment.objects.make_root_comment(
                    owner_id=serializer.validated_data['owner_id'],
                    entity_type=serializer.validated_data['entity_type'],
                    entity_id=serializer.validated_data['entity_id'],
                    text=serializer.validated_data['text'])
            return Response(self.get_serializer(comment).data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EditCommentView(RetrieveUpdateDestroyAPIView):
    """Редактирование комментария. Идентификатор комментария задаётся в URL.

    Можно редактировать комментарий другого пользователя."""

    serializer_class = CommentSerializerForEditing

    def get_queryset(self):
        return Comment.objects.all()  # @UndefinedVariable

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.is_leaf_node():
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response('Not a leaf', status=status.HTTP_400_BAD_REQUEST)


class TopLevelCommentsListView(ListAPIView):
    """Получение комментариев первого уровня с пагинацией.

    Опциональные GET-параметры, должны быть заданы вместе:

    - entity_type - типа сущности
    - entity_id - идентификатор сущности
    """
    class PaginationDemostration(LimitOffsetPagination):
        default_limit = 5

    serializer_class = CommentSerializerForReading
    pagination_class = PaginationDemostration

    def get_queryset(self):
        if 'entity_type' in self.request.GET and 'entity_id' in self.request.GET:
            return Comment.objects.get_collection(
                entity=(self.request.GET['entity_type'], self.request.GET['entity_id']),
                with_childs=False
            )
        else:
            return Comment.objects.all()  # pragma:no cover @UndefinedVariable


class ChildCommentsListView(ListAPIView):
    """Получение всех дочерних для заданного комментариев без ограничения по уровню вложенности."""

    serializer_class = CommentSerializerForReading

    def get_queryset(self):
        comment = get_object_or_404(Comment, pk=self.kwargs['pk'])
        return comment.get_descendants(include_self=False)


class CommentsTreeView(APIView):
    """Получение дерева комментариев для комментария или сущности.

    Обязательные GET-параметры:

    Либо:

    - entity_type - типа сущности И
    - entity_id - идентификатор сущности

    Либо:

    - comment_id - идентификатор комментария

    Формат ответа - список, ибо комментариев первого уровня у нас может быть несколько,
    а хранение родительской сущности для них не предусмотрено:

        [   {   'edit_url': 'http://testhost/comments/edit/8/',
                'entity_id': 1234,
                'entity_type': 'this-service',
                'id': 123,
                'owner_id': 0,
                'parent': 1,
                'text': 'good service, developer is really smart!',
                'childs': [ ITEM, ... ]}]   // ключ отсутствует, если дочерних элементов нет

    """

    def get(self, request):
        if 'entity_type' in self.request.GET and 'entity_id' in self.request.GET:
            comments = Comment.objects.get_collection(  # @UndefinedVariable
                entity=(self.request.GET['entity_type'], self.request.GET['entity_id']),
                with_childs=True
            )
        elif 'comment_id' in self.request.GET:
            comments = Comment.objects.get_collection(
                          comment_id=self.request.GET['comment_id'],
                          with_childs=True
                       )
        else:
            return Response(
                'Specify (entity_type & entity_id) or comment_id',
                status=status.HTTP_400_BAD_REQUEST
            )

        def serialize_tree(node):
            """Сериализует дерево, заданное вершиной."""
            childs_serialized = []
            result = CommentSerializerForReading(node, context={'request': self.request}).data

            for child in node.get_children():
                childs_serialized.append(serialize_tree(child))

            if childs_serialized:
                result['childs'] = childs_serialized

            return result

        roots = get_cached_trees(comments)  # Строим дерево в памяти, как mptt.templatetags.RecurseTreeNode.
        result = [serialize_tree(root) for root in roots]
        return Response(result)


class FilterByUserView(ListAPIView):
    """Получение всех комментарий пользователя."""

    serializer_class = CommentSerializerForReading

    def get_queryset(self):
        owner_id = self.kwargs['owner_id']
        return Comment.objects.get_collection(owner_id=owner_id)  # @UndefinedVariable


class CreateExportCommentView(CreateAPIView):
    """Создание объекта экспорта данных. Экспорт рекурсивен.

    Допущение: объект экспорта - это набор фильтров. Соответственно, если записи были изменены
    или удалены между выгрузками, то это приведёт к разным результатам. Создание новых записей
    не изменит результат (с точностью до секунды и если дата-времмя не менялась в прошлое).

    Почему так сделано: чтобы не возиться с асинхронными задачами и хранилищами media-файлов.
    """
    serializer_class = ExportCommentSerializer

    def get_serializer_context(self):
        context = super(CreateExportCommentView, self).get_serializer_context()
        context['format'] = 'xml'
        return context

    def perform_create(self, serializer):
        serializer.save()
        serializer.save()


class PerformExportCommentView(ListAPIView):
    """Исполнение задачи экспорта."""

    serializer_class = CommentSerializerForExport

    def get_queryset(self):
        export = get_object_or_404(ExportComment, pk=self.kwargs['pk'])
        return export.get_filtered_comments()


class ExportCommentListView(ListAPIView):
    """Просмотр истории выгрузок пользователя."""

    serializer_class = ExportCommentSerializer

    def get_serializer_context(self):
        context = super(ExportCommentListView, self).get_serializer_context()
        context['format'] = 'xml'
        return context

    def get_queryset(self):
        return ExportComment.objects.filter(owner_id=self.kwargs['pk'])
