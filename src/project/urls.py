# -*- coding: utf-8 -*-
from __future__ import (absolute_import, division, print_function, unicode_literals)

from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/comments/')),
    url(r'^admin/', admin.site.urls),
    url(r'^comments/', include('comments.urls', namespace='comments')),
]
