#!/bin/bash
cd "$(dirname "$0")"  # change to script directory
flake8 --config=flake8.conf src
coverage erase
coverage run --source='src/comments' src/manage.py test comments
coverage report
